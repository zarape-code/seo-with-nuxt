import api from './libs/api'
export default {
  ssr: true,
  head: {
    title: 'Blog demo Zarape IO',
    htmlAttrs: {
      lang: 'es-MX'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/bulma',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap'
  ],
  sitemap: {
    hostname: `${process.env.URL_SITE}`,
    lastmod: '2020-11-18',
    gzip: true,
    exclude: [
      '/_nuxt'

    ],
    routes: async () =>  {
      try {
        return api.POST.map(element => `/post/${element.path}`)
      } catch (_) {
        return []
      }
    }
  },
  robots: {
    UserAgent: '*',
    Disallow: '/_nuxt',
    Sitemap: `${process.env.URL_SITE}/sitemap.xml`
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
  }
}
