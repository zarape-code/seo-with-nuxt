export default {
  POST: [
    {
      id: 1,
      path: 'que-es-el-seo',
      name: '¿Qué es el SEO?',
      image: 'https://bulma.io/images/placeholders/1280x960.png',
      description: 'SEO (Search Engine Optimization u Optimización de Motores de Búsqueda)',
      date: '2020-10-11',
      author: 'RD Station',
      content: '<p>Con seguridad ya escuchaste hablar de Google, ¿verdad?</p><p>Entonces, cada vez que una página se publica en internet, Google (y otros buscadores) buscan cómo indexarla de manera que sea encontrada por quien la busca.</p><p>Sin embargo, existen millones de páginas siendo publicadas todos los días en internet, lo que hace que haya bastante competencia. Por lo tanto, ¿cómo hacer que una página esté al frente de otras?</p><p>Ahí es que entra el SEO (Search Engine Optimization). Como la propia traducción ya indica, SEO es una optimización para los motores de búsqueda, esto significa, un conjunto de técnicas que influencian los algoritmos de los buscadores para definir el ranking de una página para determinada palabra clave que fue procurada.</p>'
    },
    {
      id: 2,
      path: 'implementar-seo-en-un-proyecto-con-nuxtjs',
      name: 'Implementar SEO en un proyecto con Nuxt.js',
      image: 'https://bulma.io/images/placeholders/1280x960.png',
      description: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem...',
      date: '2020-10-11',
      author: 'Ulises Uribe',
      content: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem.'
    },
    {
      id: 3,
      path: 'utilizando-nuxtjs-con-ssr',
      name: 'Utilizando Nuxt.js con SSR',
      image: 'https://bulma.io/images/placeholders/1280x960.png',
      description: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem...',
      date: '2020-10-11',
      author: 'Ulises Uribe',
      content: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem.'
    }
  ]
}