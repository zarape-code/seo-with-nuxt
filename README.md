# SEO with nuxt

Blog con SEO, Nuxt.js y SSR [más info...](http://blog.zarape.io/)



## Iniciar el proyecto

```bash
# Instala las dependencias
$ npm install

# Arrancar en modo desarrollo con hot reload en localhost:3000
$ npm run dev

# Compilar y ejecuta el proyecto para production
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Estructura del proyecto
```sh
\                   # archivos de configuración
  |-components      # Carpeta para los componentes necesarios para el blog
  |-constants       # Carpeta con las constantes de los mensajes a mostrar en errores.
  |-layout          # Template base para mostrar en las páginas.
  |-libs
    |-api           # Simulación de conexión a una API utilzando un json.
  |-pages           #Contiene las vistas y rutas de su aplicación.
  |-plugins         # Este es el lugar para agregar complementos de Vue e inyectar funciones o constantes. Cada vez que necesite usar Vue.use ()
  |-nuxt.config.js  # Archivo de configuración general del proyecto
```

## Paquetes utilizados para el SEO
* [@nuxtjs/robots](https://www.npmjs.com/package/@nuxtjs/robots) - Paquete que permite generar el archivo robots.
* [@nuxtjs/sitemap](https://www.npmjs.com/package/@nuxtjs/sitemap) - Paquete que permite generar el sitemap de las páginas creadas y las rutas dinámicas.

## Tecnología empleada
* [Nuxt.js](https://nuxtjs.org) - Web Framework

## Extensiones para validar SEO en chrome
* [meta seo inspector](https://www.omiod.com/meta-seo-inspector/) - Extención para validar que los metas estén creados de manera correcta.


___
### Autor
Creado por [Ulises Uribe](https://www.linkedin.com/in/ulises-uribe/) para zarape.io 🇲🇽
