export default {
  POST: {
    NOT_FOUND: 'La ruta a la que ingresaste no existe',
    ERROR_SERVER: 'Ha ocurrido un error, intenta de nuevo más tarde'
  }
}

